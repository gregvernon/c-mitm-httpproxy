using System.IO;
using System.Threading;

namespace SimpleLogger
{
    public enum Levels {info, debug, error};

    public class Settings
    {
        public string Filename {get; set;}
        public SimpleLogger.Levels Level {get; set;}

        public Settings(string file, SimpleLogger.Levels level)
        {
            Filename = file;
            Level = level;
        }
    }

    public class Logger
    {
        private SimpleLogger.Settings _settings;
        private ReaderWriterLockSlim fileLock = new ReaderWriterLockSlim();

        public Logger(SimpleLogger.Settings Settings)
        {
            _settings = Settings;
        }

        public void Log(string Message, SimpleLogger.Levels Level = SimpleLogger.Levels.info)
        {
            if (Level < _settings.Level)
                return;

            fileLock.EnterWriteLock();
            try
            {
                using(StreamWriter sw = File.AppendText(_settings.Filename))
                {
                    var dateStr = System.DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.f");
                    sw.WriteLine("[" + dateStr + " " + Level.ToString() + "]: " + Message);
                    sw.Close();
                }

            }
            finally
            {
                fileLock.ExitWriteLock();
            }
        }
    }
}