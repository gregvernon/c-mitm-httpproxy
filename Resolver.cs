using System;
using System.Collections;
using System.IO;
using System.Net;

namespace ProxyResolver
{
    public class ResolveData
    {
        public string ForwardHost;
        public ushort ForwardPort;
        //public ushort TTL;
    }

    public class ResolveConfigHost
    {
 
        public string Host;
        public string Resolve;

        public short Port;
    }

/*
    internal class ZoneData
    {
        public string Name;
        public Dictionary<string, ResolveData> Domains;

        public ZoneData()
        {
            Domains = new Dictionary<string, ResolveData>();
        }
    }
*/
    public class Resolver
    {
        //private List<string> _currentZones;
        private SortedList _zoneData;
        private SimpleLogger.Logger _log;

        public Resolver()
        {
            // Create the logger, logging all levels at least info
            _log = new SimpleLogger.Logger(new SimpleLogger.Settings("Resolver.log", SimpleLogger.Levels.info));
            //_currentZones = new List<string>();
            _zoneData = new SortedList();

            // Read the hosts file and add the data to the resolver
            var hostsFile = Path.Combine(Directory.GetCurrentDirectory(), "resolve.hosts");
            var lineIndex = -1;
            foreach(String host in System.IO.File.ReadAllLines(hostsFile))
            {
                ++lineIndex;
                var hostData = host.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                
                short testPort;
                IPAddress testIp;
                // Make sure the host data at least looks valid
                if(
                    hostData.Length != 3 || 
                    ! Int16.TryParse(hostData[2], out testPort) || 
                    ! IPAddress.TryParse(hostData[1], out testIp)
                )
                {
                    _log.Log("Invalid host on line " + lineIndex.ToString() + ": " + host, SimpleLogger.Levels.error);
                    continue;
                }

                _zoneData.Add(hostData[0], new IPEndPoint(testIp, testPort));//(hostData[1] + ":" + hostData[2]));
            }
            /*
            Console.WriteLine(config.GetSection("hosts").Value);
            var dd = (ResolveConfigHost)config.GetSection("hosts");
            Console.WriteLine(dd);
            /*
            NameValueCollection hosts = (NameValueCollection)config.GetSection("hosts");
            foreach (ResolveConfigHost host in hosts)
            {
                Console.WriteLine(host);
            }
            */



            // create a new demo resolveable data
            //_currentZones.Add("on3pk.gq.");
            // add some data
            /*
            _zoneData.Add(".com.google", "172.217.10.14:80");
            _zoneData.Add(".gq.on3pk.localhost", "63.223.112.91:80");
            _zoneData.Add(".gq.on3pk.*", "63.223.112.91:80");
            */
            //_zoneData["on3pk.gq"].Domains.Add("extrazone.on3pk.gq", new ResolveData());
        }

        public bool TryResolve(string host, out IPEndPoint result)
        {
            // result = new ResolveData();
            // split the host into parts
            string[] parts = host.Split('.', StringSplitOptions.RemoveEmptyEntries);

            foreach(var part in parts)
            {
                Console.WriteLine(part);
            }

            string testHost = "";

            result = null;
            bool foundHost = false;
            for(int i = 1; i <= parts.Length; ++i)
            {
                string newSection = parts[(parts.Length - i)];
                
                Console.WriteLine("searching for: " + testHost + ".*");

                // check non-wild card result
                if(_zoneData.ContainsKey(testHost + "." + newSection))
                {
                    foundHost = true;
                    result = (IPEndPoint)_zoneData[testHost + "." + newSection];
                    break;
                }

                // check wildcard result
                if(_zoneData.ContainsKey(testHost + ".*"))
                {
                    foundHost = true;
                    result = (IPEndPoint)_zoneData[testHost + ".*"];
                    break;
                }

                testHost +=  "." + newSection;// + "." + testHost;
            }

            if(! foundHost)
            {
                //result = new ResolveData();
                return false;
            }
            
            Console.WriteLine("found host!");
            /*
            parts = forwardResult.Split(":", StringSplitOptions.RemoveEmptyEntries);
            result.ForwardHost = parts[0];
            if(! UInt16.TryParse(parts[1], out result.ForwardPort))
            {
                _log.Log("Unable to parse forward port", SimpleLogger.Levels.error);
            }
            */
            return true;
        }
    }
}