using System;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace socketApp
{
    class Server
    {
        private ProxyResolver.Resolver _resolver;
        private SimpleLogger.Logger _log;
        private TcpListener _listener;

        public Server()
        {
            // Create the logger
            _log = new SimpleLogger.Logger(new SimpleLogger.Settings("Server.log", SimpleLogger.Levels.info));
            _log.Log("Server Started.");

            // Create the resolver
            _resolver = new ProxyResolver.Resolver();

            // Finally, start the listener
            this._listener = new TcpListener(new IPEndPoint(IPAddress.Any, 8080));
            _listener.Start();

            // Start accepting clients!
            BeginAccept();
        }

        private void BeginAccept()
        {
            try
            {
                // Start accepting new clients on the listener
                _listener.BeginAcceptTcpClient(ClientConnectionCallback, _listener);
            }
            catch(Exception e)
            {
                // log the exception
                _log.Log("Exception occurred: " + e.ToString());
                // Stop the listener
                _listener.Stop();
            }
        }

        public void ClientConnectionCallback(IAsyncResult ar)
        {
            if(_listener.Server.IsBound)
                // Ensure clients can still connect
                BeginAccept();

            // Accept the client
            TcpClient client = _listener.EndAcceptTcpClient(ar);
            Task.Run(() => HandleClient(client));

            return;
            TcpClient remote = new TcpClient();
            remote.Connect(System.Net.IPAddress.Parse("63.223.112.91"), 80);
            NetworkStream remoteStream = remote.GetStream();
            NetworkStream stream = client.GetStream();

            Byte[] data = new Byte[256];

            // String to store the response ASCII representation.
            String responseData = String.Empty;

            // Read the first batch of the TcpServer response bytes.
            while (stream.DataAvailable)
            {
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData += System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                //Console.WriteLine("Received: {0}", responseData);
                //Console.WriteLine("Data available..");
            }

            Byte[] iRequest = System.Text.Encoding.ASCII.GetBytes(responseData);
            remoteStream.Write(iRequest, 0, iRequest.Length);
            //remoteStream

            responseData = String.Empty;
            while(remoteStream.DataAvailable)
            {
                Int32 bytes = remoteStream.Read(data, 0, data.Length);
                responseData += System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            }

            Console.WriteLine(responseData);

            /*String html = "<b>Error 501</b>";
            String resp = "HTTP/1.1 200 OK" + Environment.NewLine; 
            resp += "Date: Mon, 27 Jul 2009 12:28:53 GMT" + Environment.NewLine;
            resp += "Server: Apache/2.2.14 (Win32)" + Environment.NewLine;
            resp += "Content-Length: 0" + Environment.NewLine;
            resp += "Content-Type: text/html" + Environment.NewLine;
            resp += "Connection: Closed" + Environment.NewLine;

            //resp += html;
            */

            remoteStream.Flush();
            remoteStream.Close();

            stream.Flush();
            //byte[] myWriteBuffer = System.Text.Encoding.ASCII.GetBytes(resp);
            //stream.Write(myWriteBuffer, 0, myWriteBuffer.Length);
            stream.Close();
            client.Close();
            //Console.WriteLine("Client Connected");
        }

        private void HandleClient(TcpClient client)
        {
            _log.Log("New request received from " + client.Client.RemoteEndPoint.ToString());
            _log.Log("Starting Tunnel Thread", SimpleLogger.Levels.info);
            //TcpClient remote = new TcpClient();//"63.223.112.91"), 80);
            //remote.Connect(System.Net.IPAddress.Parse("63.223.112.91"), 80);

            
            
            NetworkStream clientStream = client.GetStream();
            var httpRequest = getHttpPacket(clientStream);

            /*
            if(msg.Trim() == String.Empty)
            {
                Console.WriteLine("Empty Connection");
                return;
            }
            */
            
            if( ! httpRequest.ToUpper().Contains("HOST:"))
            {
                Console.WriteLine("Invalid connection...");

                _log.Log("Invalid connection request received from " + client.Client.RemoteEndPoint.ToString());
                httpResponse(client, 503);
                closeConnection(client);
                return;
            }
            
            var httpHost = getHttpHost(httpRequest);
            var uri = new Uri(@"http://" + httpHost);
            Console.WriteLine("Requested Host: " + uri.DnsSafeHost);
            IPEndPoint _dd;
            if(! _resolver.TryResolve(uri.DnsSafeHost, out _dd))
            {
                _log.Log("Unable to find forward direction", SimpleLogger.Levels.info);
                httpResponse(client, 501);
                closeConnection(client);
                return;
            }

            Console.WriteLine(_dd.ToString());
            /*
            Console.WriteLine(uri.DnsSafeHost + ": " + uri.HostNameType.ToString() + ": " + uri.Host);
            Console.WriteLine("Host: " + httpHost);
            Console.WriteLine(uri.GetLeftPart(UriPartial.Authority));
            */
            //httpRequest = httpRequest.Replace("localhost:8080", "google.com");

            // Connect to the remote host
            var remote = new TcpClient();
            remote.Connect(_dd.Address, _dd.Port);
            //remote.Connect("172.217.10.110", 80);
            var bb = System.Text.Encoding.ASCII.GetBytes(httpRequest);

            Console.WriteLine(httpRequest);

            remote.GetStream().Write(bb, 0, bb.Length);

            // Copy data from stream to stream
            TunnelStreams(clientStream, remote.GetStream());

            
            closeConnection(client);
            closeConnection(remote);

            return;

            // get the host
            var msg= httpRequest;
            var host = "";
            var hostIndex = msg.ToUpper().IndexOf("HOST: ") + 6;
            while(! Char.IsControl(msg.ToCharArray()[hostIndex]))
            {
                host += msg.ToCharArray()[hostIndex].ToString();
                hostIndex++;
            }

            Console.WriteLine(host);

            if (msg.ToUpper().IndexOf("HOST: ") == -1)
            {

            }
            //NetworkStream remoteStream = remote.GetStream();

            Console.WriteLine(msg);

            clientStream.Flush();
            clientStream.Close();

            client.Close();


        }

        private void TunnelStreams(NetworkStream origin, NetworkStream remote)
        {
            System.Diagnostics.Stopwatch _timer = System.Diagnostics.Stopwatch.StartNew();
            while(origin.CanRead && origin.CanWrite && remote.CanWrite) {
                if(remote.DataAvailable)
                {
                    remote.CopyTo(origin);
                }

                if(origin.DataAvailable)
                {
                    origin.CopyTo(remote);
                }

                Console.WriteLine("Tunneling on thread: " + Thread.CurrentThread.ManagedThreadId.ToString());
            }

            _timer.Stop();
            Console.WriteLine("Milliseconds in TN Operation: " + _timer.ElapsedMilliseconds.ToString());
        }

        // close a TcpClient connection
        private void closeConnection(TcpClient client)
        {
            client.GetStream().Flush();
            client.GetStream().Close();
            client.Close();
        }

        private string getHttpPacket(NetworkStream stream)
        {
            string httpRequest = "";
            bool clientSending = true;
            while(clientSending)
            {
                // read data from the client
                Byte[] buffer = new Byte[1024];
                var len = stream.Read(buffer, 0, buffer.Length);
                httpRequest += System.Text.Encoding.ASCII.GetString(buffer, 0, len);
                
                // Check to see if the http request has been terminated
                for(var byteIndex = 1; byteIndex <= 4; ++byteIndex)
                {
                    // HTTP communications are terminated by \r\n\r\n
                    // check to see if there are four control chars in a row
                    if(! Char.IsControl(httpRequest, httpRequest.Length - byteIndex))
                    {
                        clientSending = true;
                        break;
                    } else {
                        clientSending = false;
                    }
                } // for
            } // while
            return httpRequest;
        }

        private string getHttpHost(string httpRequest)
        {
            var host = "";
            var hostIndex = httpRequest.ToUpper().IndexOf("HOST: ") + 6;
            while(! Char.IsControl(httpRequest.ToCharArray()[hostIndex]))
            {
                host += httpRequest.ToCharArray()[hostIndex].ToString();
                hostIndex++;
            }

            return host.Trim();
        }

        private void httpResponse(TcpClient client, int HttpResponseCode)
        {
            _log.Log("Returning local HTTP response due to request error", SimpleLogger.Levels.info);

            var message = "HTTP/1.1 " + HttpResponseCode.ToString();
            var messageAsBytes = System.Text.Encoding.ASCII.GetBytes(message);
            client.GetStream().Write(messageAsBytes, 0, messageAsBytes.Length);
        }
    }

}